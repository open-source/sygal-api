<?php

return [
    'service_manager' => [
        'factories' => [
            \SygalApi\V1\Rest\Ping\PingResource::class => \SygalApi\V1\Rest\Ping\PingResourceFactory::class,
            \SygalApi\V1\Rest\Diplomation\DiplomationResource::class => \SygalApi\V1\Rest\Diplomation\DiplomationResourceFactory::class,
            \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeResource::class => \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeResourceFactory::class,
            \SygalApi\V1\Rest\Doctorant\DoctorantResource::class => \SygalApi\V1\Rest\Doctorant\DoctorantResourceFactory::class,
        ],
    ],
    'openapi_content_validation' => [
        // 'controller' => 'validator'
        'SygalApi\V1\Rest\InscriptionAdministrative\Controller' => 'SygalApi\V1\Rest\InscriptionAdministrative\OpenApiValidator',
        'SygalApi\V1\Rest\Doctorant\Controller' => 'SygalApi\V1\Rest\Doctorant\OpenApiValidator',
    ],
    'openapi_validator_specs' => [
        // 'validator' => [config]
        'SygalApi\V1\Rest\InscriptionAdministrative\OpenApiValidator' => [
            'oas_file_format' => 'yaml',
            'oas_file_path' => __DIR__ . '/schema/inscription/doctorant-api-v1.yaml',
        ],
        'SygalApi\V1\Rest\Doctorant\OpenApiValidator' => [
            'oas_file_format' => 'yaml',
            'oas_file_path' => __DIR__ . '/schema/doctorant/doctorant-api-v1.yaml',
        ],
    ],
    'router' => [
        'routes' => [
            'api.rest.ping' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/ping[/:ping_id]',
                    'defaults' => [
                        'controller' => 'SygalApi\\V1\\Rest\\Ping\\Controller',
                    ],
                ],
            ],
            'api.rest.diplomation' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/diplomation[/:diplomation_id]',
                    'defaults' => [
                        'controller' => 'SygalApi\\V1\\Rest\\Diplomation\\Controller',
                    ],
                ],
            ],
            'api.rest.inscription' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/inscription-administrative[/:inscription_id]',
                    'defaults' => [
                        'controller' => 'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller',
                    ],
                ],
            ],
            'api.rest.doctorant' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/doctorant[/:doctorant_id]',
                    'defaults' => [
                        'controller' => 'SygalApi\\V1\\Rest\\Doctorant\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-rest' => [
        'SygalApi\\V1\\Rest\\Ping\\Controller' => [
            'listener' => \SygalApi\V1\Rest\Ping\PingResource::class,
            'route_name' => 'api.rest.ping',
            'route_identifier_name' => 'ping_id',
            'collection_name' => 'ping',
            'entity_http_methods' => [
                'GET' => 'GET', // astuce pour pouvoir "désactiver" la méthode si besoin
//                1 => 'PATCH',
//                2 => 'PUT',
//                3 => 'DELETE',
            ],
            'collection_http_methods' => [
//                0 => 'GET',
//                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \SygalApi\V1\Rest\Ping\PingEntity::class,
            'collection_class' => \SygalApi\V1\Rest\Ping\PingCollection::class,
            'service_name' => 'Ping',
        ],
        'SygalApi\\V1\\Rest\\Diplomation\\Controller' => [
            'listener' => \SygalApi\V1\Rest\Diplomation\DiplomationResource::class,
            'route_name' => 'api.rest.diplomation',
            'route_identifier_name' => 'diplomation_id',
            'collection_name' => 'diplomation',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \SygalApi\V1\Rest\Diplomation\DiplomationEntity::class,
            'collection_class' => \SygalApi\V1\Rest\Diplomation\DiplomationCollection::class,
            'service_name' => 'Diplomation',
        ],
        'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller' => [
            'listener' => \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeResource::class,
            'route_name' => 'api.rest.inscription',
            'route_identifier_name' => 'inscription_id',
            'collection_name' => 'inscription',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeEntity::class,
            'collection_class' => \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeCollection::class,
            'service_name' => 'InscriptionAdministrative',
        ],
        'SygalApi\\V1\\Rest\\Doctorant\\Controller' => [
            'listener' => \SygalApi\V1\Rest\Doctorant\DoctorantResource::class,
            'route_name' => 'api.rest.doctorant',
            'route_identifier_name' => 'doctorant_id',
            'collection_name' => 'doctorant',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \SygalApi\V1\Rest\Doctorant\DoctorantEntity::class,
            'collection_class' => \SygalApi\V1\Rest\Doctorant\DoctorantCollection::class,
            'service_name' => 'Doctorant',
        ],
    ],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'SygalApi\\V1\\Rest\\Ping\\Controller' => 'Json',
            'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller' => 'Json',
            'SygalApi\\V1\\Rest\\Doctorant\\Controller' => 'Json',
            'SygalApi\\V1\\Rest\\Diplomation\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'SygalApi\\V1\\Rest\\Ping\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                2 => 'application/json',
            ],
            'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                2 => 'application/json',
            ],
            'SygalApi\\V1\\Rest\\Doctorant\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                2 => 'application/json',
            ],
            'SygalApi\\V1\\Rest\\Diplomation\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'SygalApi\\V1\\Rest\\Ping\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
            'SygalApi\\V1\\Rest\\Diplomation\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
            'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
            'SygalApi\\V1\\Rest\\Doctorant\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-hal' => [
        'metadata_map' => [
            \SygalApi\V1\Rest\Ping\PingEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.ping',
                'route_identifier_name' => 'ping_id',
                'hydrator' => \Laminas\Hydrator\ObjectPropertyHydrator::class,
            ],
            \SygalApi\V1\Rest\Ping\PingCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.ping',
                'route_identifier_name' => 'ping_id',
                'is_collection' => true,
            ],
            \SygalApi\V1\Rest\Diplomation\DiplomationEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.diplomation',
                'route_identifier_name' => 'diplomation_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializableHydrator::class,
            ],
            \SygalApi\V1\Rest\Diplomation\DiplomationCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.diplomation',
                'route_identifier_name' => 'diplomation_id',
                'is_collection' => true,
            ],
            \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.inscription',
                'route_identifier_name' => 'inscription_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializableHydrator::class,
            ],
            \SygalApi\V1\Rest\InscriptionAdministrative\InscriptionAdministrativeCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.inscription',
                'route_identifier_name' => 'inscription_id',
                'is_collection' => true,
            ],
            \SygalApi\V1\Rest\Doctorant\DoctorantEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctorant',
                'route_identifier_name' => 'doctorant_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializableHydrator::class,
            ],
            \SygalApi\V1\Rest\Doctorant\DoctorantCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctorant',
                'route_identifier_name' => 'doctorant_id',
                'is_collection' => true,
            ],
        ],
    ],
    'api-tools-content-validation' => [
        'SygalApi\\V1\\Rest\\Diplomation\\Controller' => [
            'input_filter' => 'SygalApi\\V1\\Rest\\Diplomation\\Validator',
        ],
        'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller' => [
            'input_filter' => 'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Validator',
        ],
        'SygalApi\\V1\\Rest\\Doctorant\\Controller' => [
            'input_filter' => 'SygalApi\\V1\\Rest\\Doctorant\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'SygalApi\\V1\\Rest\\Diplomation\\Validator' => [],
        'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Validator' => [],
        'SygalApi\\V1\\Rest\\Doctorant\\Validator' => [],
    ],
    'api-tools-mvc-auth' => [
        'authorization' => [
            'SygalApi\\V1\\Rest\\Ping\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
            'SygalApi\\V1\\Rest\\Diplomation\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
            'SygalApi\\V1\\Rest\\InscriptionAdministrative\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
            'SygalApi\\V1\\Rest\\Doctorant\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
        ],
    ],
];
