openapi: '3.0.1'
info:
  description: |
    Il s'agit de l'API présentée par l'application de 3ème cycle pour gérer les échanges avec Pegase. 
    Dans cette version initiale une seul flux est géré

    __Inscriptions administratives :__ ce flux permet de pousser les inscriptions administratives validées des doctorants de Pegase vers l'application de 3ème cycle
    La saisie d'une école doctorale est l'information qui permet de considérer une inscription administrative comme celle d'un doctorant.
  title: Connecteur 3eme cycle pour Pegase v0
  version: '0.1'
servers:
  - url: /api/v1
paths:
  /inscription-administrative:
   post:
      tags:
      - doctorant
      summary: Ajout d'une inscription administrative
      description: Ajout d'une inscription administrative
      operationId: creerInscriptionAdministrative
      requestBody:
        description: Les informations de l'inscription administrative pour le doctorant (en utf-8)
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InscriptionAdministrative'
      security:
      - idTokenAuth: []
      responses:
        201:
          description: Inscription administrative reçue (body vide, car réservé aux erreurs)
          content:
              application/json:
                schema:
                  type: string
        403:
          description: non autorise (détails de l'erreur dans le body - max 2000 caractères)
          content:
              application/json:
                schema:
                  type: string
        422:
          description: donnees invalides (détails de l'erreur dans le body - max 2000 caractères)
          content:
              application/json:
                schema:
                  type: string
        500:
          description: erreur serveur (détails de l'erreur dans le body - max 2000 caractères)
          content:
              application/json:
                schema:
                  type: string
components:
 securitySchemes:
  # Authorization: Bearer id.token.
  idTokenAuth:
    type: http
    scheme: basic
    description: connexion avec login / mot de passe.
 schemas:
  InscriptionAdministrative:
    type: object
    required:
      - id
      - instancePegase
      - apprenant
      - inscription
    properties:
      id:
        type: string
        description: identifiant Pegase unique de l'inscription. Ceci permettra à l'appli 3e cycle d'identifier l'inscription
        example: codeEtablissement@codeApprenant@codeChemin@codePeriode (ETAB00@2023004@LIC-MAI>LIC-MAI-L1@PER-2023)
      instancePegase:
        type: string
        description: identifiant de l'instance de Pegase émettrice. Ceci servira lors des flux retour
        example: uphf (Université Polytechnique des Hauts de France), univ-jfc (INUC), etc.
      apprenant:
        $ref: '#/components/schemas/Apprenant'
      # dans cette version l'ensemble des incriptions de l'apprenant pour la période est transmis. VOIR SI ON LIMITE A L'INSCRIPTION EN DOCTORANT
      inscription:
        description: l'inscription validée ou annulée.
        $ref: '#/components/schemas/Inscription'
  Apprenant:
    type: object
    description: Les données de l'apprenant
    required:
      - code
      - INE
      - nomDeNaissance
      - nomUsuel
      - prenom
      - genre
      - naissance
    properties:
      code:
        type: string
        description: le code Pegase de l'apprenant
        example: 2023004
      INE:
        type: string
        description: Le numéro INE de l'apprenant si connu
      nomDeNaissance:
        type: string
        description: Le nom de naissance
      nomUsuel:
        type: string
        description: Le nom usuel
      prenom:
        type: string
        description: Le prénom
      deuxiemePrenom:
        type: string
        nullable: true
        description: Le deuxième prénom
      troisiemePrenom:
        type: string
        nullable: true
        description: Le troisième prénom
      prenomUsage:
        type: string
        nullable: true
        description: Le prénom d'usage (vide pour le moment)
      genre:
        type: string
        enum:
          - F
          - M
        nullable: true
        description: Les valeurs `M` ou `F` sont acceptées
      naissance:
        $ref: '#/components/schemas/Naissance'
      mailPersonnel:
        type: string
        format: email
        nullable: true
        description: Adresse de messagerie électronique personnelle
      mailUrgence:
        type: string
        format: email
        nullable: true
        description: Adresse de messagerie électronique d'urgence (vide pour le moment)
      telephonePersonnel:
        type: string
        nullable: true
        description: Numéro de téléphone personnel
      telephoneUrgence:
        type: string
        nullable: true
        description: Numéro de téléphone d'urgence
      adresseFixeHorsPeriodeUniversitaire:
        description: Adresse postale hors periode universitaire
        $ref: '#/components/schemas/Adresse'
      adresseResidenceUniversitaire:
        description: Adresse postale universitaire
        $ref: '#/components/schemas/Adresse'
  Inscription:
    type: object
    required:
      - noCandidat
      - origine
      - codeStructureEtablissementDuChemin
      - chemin
      - periode
      - statutInscription
      - cesure
      - mobilite
      - ecoleDoctorale
      - dateInscription
    properties:
      noCandidat:
        type: string
        description: Le numéro de la candidature associé à l'origine de la candidature.
      origine:
        description: L'origine de la candidature de l'étudiant.
        enum:
        - CONCOURS
        - PARCOURSUP
        - ECANDIDAT
        - MANUEL
        - REINSCRIPTION
        - MONMASTER
      formation:
        type: string
        description: le libellé de la formation racine de la maquette de formation.
        example: Licence de Mathématiques et d'Informatiques (code LIC-MAI)
        nullable: true
      chemin:
        type: string
        description: liste des codes ObjetFormation ou Groupement depuis la Formation racine incluse.
        example: LIC-MAI>LIC-MAI-L1
      codeStructureEtablissementDuChemin:
        type: string
        description: Le code structure Pegase de l’établissement pour le chemin.
        example: ETAB00
      periode:
        $ref: '#/components/schemas/Periode'
      principale:
        type: boolean
        description: le temoin indiquant si une inscription est principale ou pas
      statutInscription:
        description: Le statut de l'inscription
        type: string
        enum:
        - VALIDE
        - ANNULEE
      regimeInscription:
        $ref: '#/components/schemas/RegimeInscription'
      boursesEtAides:
        type: array
        items:
          $ref: '#/components/schemas/BourseOuAideFinanciere'
        description: Les bourses et aides financières
      cesure:
        type: string
        enum:
          - SANS
          - SEMESTRE
          - ANNEE
        description: Indique si le doctorant est en césure
      mobilite:
        type: string
        enum:
          - SANS
          - ENTRANTE
          - SORTANTE
        description: Indique si le doctorant est en mobilité
      ecoleDoctorale:
        type: string
        description: code de la nomenclature école doctorale
        pattern: '^[1-9][0-9]*$'
        example: 497 (pour l'ED NBISE)
      dateInscription:
        type: string
        format: date
        description: La date de l'inscription au format AAAA-MM-JJ
      dateAnnulation:
        type: string
        format: date
        nullable: true
        description: La date d'annulation de l'inscription au format AAAA-MM-JJ

  Naissance:
    type: object
    required:
      - dateDeNaissance
      - paysDeNaissance
      - nationalite
    properties:
      dateDeNaissance:
        type: string
        format: date
        description: La date de naissance au format AAAA-MM-JJ
      paysDeNaissance:
        type: string
        pattern: '^[A-Z]{2}$'
        description: Le code pays du pays de naissance (code ISO-alpha 2)
        example: 'FR'
      libellePaysDeNaissance:
        type: string
        description: Le libelle du pays de naissance
        example: France
      communeDeNaissance:
        type: string
        description: Le code INSEE de la commune de naissance en France
        nullable: true
        example: 67482
      libelleCommuneDeNaissance:
        type: string
        description: Le libelle de la commune de naissance en France
        nullable: true
        example: Strasbourg
      communeDeNaissanceEtranger:
        type: string
        nullable: true
        description: La commune de naissance à l'étranger
      nationalite:
        type: string
        pattern: '^[A-Z]{2}$'
        description: Le code pays associé à la nationalité (code ISO-alpha 2)
        example: 'FR'
      libelleNationalite:
        type: string
        description: Le libelle pays associé à la nationalité issu de la nomenclature Pays et Nationalités
      deuxiemeNationalite:
        type: string
        pattern: '^[A-Z]{2}$'
        nullable: true
        description: Le code pays associé à la deuxième nationalité (code ISO-alpha 2)
        example: 'DE'
      libelleDeuxiemeNationalite:
        type: string
        nullable: true
        description: Le libelle pays associé à la deuxième nationalité issu de la nomenclature Pays et Nationalités
        example: 'ALLEMAGNE'
      dateDObtentionDeLaDeuxiemeNationalite:
        type: string
        format: date
        nullable: true
        description: La date d'obtention de la deuxième nationalité au format AAAA-MM-JJ

  Adresse:
    type: object
    nullable: true
    required:
      - pays
    properties:
      pays:
        type: string
        pattern: '^[A-Z]{2}$'
        description: Code ISO-alpha 2 du pays (FR pour la France, sinon l'adresse est à l'étranger)
        example: 'FR'
      libellePays:
        type: string
        description: libelle du pays
      ligne1OuEtage:
        type: string
        nullable: true
        description: Le n°app/étage/couloir/esc/chez ou ligne 1 de l'adresse étrangère
      ligne2OuBatiment:
        type: string
        nullable: true
        description: L'entrée/bâtiment/immeuble/résidence ou ligne 2 de l'adresse étrangère
      ligne3OuVoie:
        type: string
        nullable: true
        description: Le numéro et le libellé de la voie ou ligne 3 de l'adresse étrangère
      ligne4OuComplement:
        type: string
        nullable: true
        description: Le lieu-dit ou service particulier de distribution ou ligne 4 de l'adresse étrangère
      ligne5Etranger:
        type: string
        nullable: true
        description: Ligne 5 de l'adresse étrangère (vide si adresse en France)
      codePostal:
        type: string
        nullable: true
        description: Le code postal de l'adresse en France (vide si adresse étrangère)
      codeInsee:
        type: string
        nullable: true
        description: Le code INSEE de la commune de l'adresse en France (vide si adresse étrangère)
      libelleCommune:
        type: string
        nullable: true
        description: Le libelle de la commune de l'adresse en France (vide si adresse étrangère)

  Periode:
    description: La période de mise en œuvre associée à la formation
    type: object
    required:
      - code
      - libelle
      - anneeUniversitaire
    properties:
      code:
        type: string
        description: le code de la période
        example: PER-2023
      libelle:
        type: string
        description: Le libellé de la période de mise en œuvre.
      anneeUniversitaire:
        type: integer
        description: L'année universitaire associée à la période au format AAAA.

  BourseOuAideFinanciere:
    type: object
    description: bourse ou aide financiere
    required:
      - code
      - libelle
    properties:
      code:
        type: string
        description: le code Pegase
        example: AID001
      libelle:
        type: string
        description: le libellé d'affichage
        example: Bourse sur critères sociaux

  RegimeInscription:
    type: object
    description: régime d'inscription
    required:
      - code
      - libelle
    properties:
      code:
        type: string
        description: le code Pegase
        example: REG001
      libelle:
        type: string
        description: le libellé d'affichage
        example: Formation initiale
