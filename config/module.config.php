<?php

namespace SygalApi;

use SygalApi\Validator\OpenApi\OpenApiValidatorFactory;
use SygalApi\Validator\OpenApi\OpenApiContentValidationManagerFactory;
use SygalApi\Validator\OpenApi\OpenApiContentValidationManager;

return [
    'service_manager' => [
        'factories' => [
            OpenApiContentValidationManager::class => OpenApiContentValidationManagerFactory::class,
        ],
    ],
    'validators' => [
        'abstract_factories' => [
            OpenApiValidatorFactory::class,
        ]
    ],
    'api-tools-versioning' => [
        'default_version' => 1,
        'uri' => [
            0 => 'api.rest.ping',
            1 => 'api.rest.diplomation',
            2 => 'api.rest.inscription',
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'api-tools-documentation-swagger' => __DIR__ . '/../view',
        ],
    ],
    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __DIR__ . '/../asset',
            ],
        ],
    ],
];
