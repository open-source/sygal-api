<?php
namespace SygalApi;

use Laminas\ApiTools\Provider\ApiToolsProviderInterface;
use Laminas\EventManager\EventInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\Stdlib\ArrayUtils;
use SygalApi\Validator\OpenApi\OpenApiContentValidationManager;

class Module implements ApiToolsProviderInterface, BootstrapListenerInterface
{
    /**
     * @param \Laminas\Mvc\MvcEvent $e
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onBootstrap(EventInterface $e)
    {
        $eventManager = $e->getApplication()->getEventManager();

        /** @var \SygalApi\Validator\OpenApi\OpenApiContentValidationManager $openApiContentValidationManager */
        $openApiContentValidationManager = $e->getApplication()->getServiceManager()->get(OpenApiContentValidationManager::class);
        $openApiContentValidationManager->attach($eventManager);
    }

    public function getConfig(): array
    {
        $configDir = __DIR__ . '/../config';

        return ArrayUtils::merge(
            include $configDir . '/module.config.php',
            include $configDir . '/v1/api.config.php',
        );
    }

    public function getAutoloaderConfig(): array
    {
        return [
            'Laminas\ApiTools\Autoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__,
                ],
            ],
        ];
    }
}
