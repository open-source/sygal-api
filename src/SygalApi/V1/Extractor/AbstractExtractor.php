<?php

namespace SygalApi\V1\Extractor;

use Laminas\Hydrator\ExtractionInterface;
use stdClass;
use Webmozart\Assert\Assert;

abstract class AbstractExtractor implements ExtractionInterface
{
    public function extract(object $object): array
    {
        Assert::isInstanceOf($object, stdClass::class);

        return [
            'source_id' => $this->extractSourceId($object),
            'source_code' => $this->extractSourceCode($object),
        ];
    }

    protected function extractSourceId(stdClass $object)
    {
        return $object->instancePegase;
    }

    abstract protected function extractSourceCode(stdClass $object): string;
}