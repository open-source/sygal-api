<?php
namespace SygalApi\V1\Rest\Ping;

class PingEntity
{
    public string $id;
    public string $date;

    public function __construct()
    {
        $this->id = uniqid();
        $this->date = date('c');
    }
}
