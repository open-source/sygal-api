<?php
namespace SygalApi\V1\Rest\Doctorant;

use Psr\Container\ContainerInterface;

class DoctorantResourceFactory
{
    public function __invoke(ContainerInterface $container): DoctorantResource
    {
        return new DoctorantResource();
    }
}
