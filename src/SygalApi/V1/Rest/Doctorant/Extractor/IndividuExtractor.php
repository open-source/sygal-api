<?php

namespace SygalApi\V1\Rest\Doctorant\Extractor;

use Individu\Entity\Db\Individu;
use stdClass;
use SygalApi\V1\Extractor\AbstractExtractor;
use Webmozart\Assert\Assert;

class IndividuExtractor extends AbstractExtractor
{
    public function extract(object $object): array
    {
        $data = parent::extract($object);

        /** @var array $naissance */
        $naissance = $object->naissance;

        return array_merge($data, [
            'type' => 'doctorant',
            'civ' => ['M'=>Individu::CIVILITE_M, 'F'=>Individu::CIVILITE_MME][$object->genre] ?? null,
            'lib_nom_usu_ind' => $object->nomUsuel,
            'lib_nom_pat_ind' => $object->nomDeNaissance,
            'lib_pr1_ind' => $object->prenom,
            'lib_pr2_ind' => $object->deuxiemePrenom ?? null,
            'lib_pr3_ind' => $object->troisiemePrenom ?? null,
            'email' => $object->mailPersonnel ?? null,
            'dat_nai_per' => $naissance['dateDeNaissance'],
            //'codepaysnationalite' => $naissance['nationalite'], // todo : colonne 'codepaysnationalite' obsolète
            'cod_pay_nat' => $naissance['nationalite'],
            'lib_nat' => $naissance['libelleNationalite'] ?? null,
            'supann_id' => null, // todo : quid ?
            'source_insert_date' => null,
            /*
            'code' => $this->>val($object, 'code')], // code apprenant pegase
            'INE' => $this->>val($object, 'INE')],
            'deuxieme_prenom' => $this->>val($object, 'deuxiemePrenom')],
            'genre' => $this->>val($object, 'genre')],
            'mail_personnel' => $this->>val($object, 'mailPersonnel')],
            'mail_urgence' => $this->>val($object, 'mailUrgence')],
            'nom_de_naissance' => $this->>val($object, 'nomDeNaissance')],
            'nom_usuel' => $this->>val($object, 'nomUsuel')],
            'prenom' => $this->>val($object, 'prenom')],
            'prenom_usage' => $this->>val($object, 'prenomUsage')],
            'telephone_personnel' => $this->>val($object, 'telephonePersonnel')],
            'telephone_urgence' => $this->>val($object, 'telephoneUrgence')],
            'troisieme_prenom' => $this->>val($object, 'troisiemePrenom')],
            //
            'commune_de_naissance' => $naissance['communeDeNaissance'],
            'commune_de_naissance_etranger' => $naissance['communeDeNaissanceEtranger'],
            'date_d_obtention_de_la_deuxieme_nationalite' => $naissance['dateDObtentionDeLaDeuxiemeNationalite'],
            'date_de_naissance' => $naissance['dateDeNaissance'],
            'deuxieme_nationalite' => $naissance['deuxiemeNationalite'],
            'libelle_commune_de_naissance' => $naissance['libelleCommuneDeNaissance'],
            'libelle_deuxieme_nationalite' => $naissance['libelleDeuxiemeNationalite'],
            'libelle_nationalite' => $naissance['libelleNationalite'],
            'libelle_pays_de_naissance' => $naissance['libellePaysDeNaissance'],
            'nationalite' => $naissance['nationalite'],
            'pays_de_naissance' => $naissance['paysDeNaissance'],
            */
        ]);
    }

    protected function extractSourceCode(stdClass $object): string
    {
        Assert::propertyExists($object, 'INE', "Clé %s introuvable dans les données apprenant");

        $ine = $object->INE;
        Assert::notEmpty($ine, "L'INE dans les données apprenant ne doit pas être vide");

        return $ine;
    }
}