<?php

namespace SygalApi\V1\Rest\Doctorant\Extractor;

use stdClass;
use SygalApi\V1\Extractor\AbstractExtractor;
use Webmozart\Assert\Assert;

class DoctorantExtractor extends AbstractExtractor
{
    public function extract(object $object): array
    {
        $data = parent::extract($object);

        $individuSourceCode = $object->INE;

        return array_merge($data, [
            'individu_id' => $individuSourceCode,
            'code_apprenant_in_source' => $object->code, // id métier de l'apprenant dans la source d'import = code apprenant pegase
            //
            'ine' => $object->INE,
            'source_insert_date' => null,
        ]);
    }

    protected function extractSourceCode(stdClass $object): string
    {
        Assert::propertyExists($object, 'INE', "Clé %s introuvable dans les données apprenant");

        $ine = $object->INE;
        Assert::notEmpty($ine, "L'INE dans les données apprenant ne doit pas être vide");

        return $ine;
    }
}