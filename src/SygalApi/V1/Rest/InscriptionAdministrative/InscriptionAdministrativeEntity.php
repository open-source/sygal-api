<?php
namespace SygalApi\V1\Rest\InscriptionAdministrative;

use stdClass;

class InscriptionAdministrativeEntity
{
    public string $instancePegase;
    public stdclass $apprenant;
    public stdclass $inscription;

}
