<?php
namespace SygalApi\V1\Rest\InscriptionAdministrative;

use Psr\Container\ContainerInterface;

class InscriptionAdministrativeResourceFactory
{
    public function __invoke(ContainerInterface $container): InscriptionAdministrativeResource
    {
        return new InscriptionAdministrativeResource();
    }
}
