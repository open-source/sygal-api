<?php

namespace SygalApi\V1\Rest\InscriptionAdministrative\Extractor;

use Individu\Entity\Db\Individu;
use Laminas\Hydrator\ExtractionInterface;
use stdClass;
use SygalApi\V1\Extractor\AbstractExtractor;
use Webmozart\Assert\Assert;

class IndividuExtractor extends AbstractExtractor
{
    public function extract(object $object): array
    {
        $data = parent::extract($object);

        /** @var array $apprenant */
        $apprenant = $object->apprenant;
        /** @var array $naissance */
        $naissance = $apprenant['naissance'];

        return array_merge($data, [
            'type' => 'doctorant',
            'civ' => ['M'=>Individu::CIVILITE_M, 'F'=>Individu::CIVILITE_MME][$apprenant['genre']] ?? null,
            'lib_nom_usu_ind' => $apprenant['nomUsuel'],
            'lib_nom_pat_ind' => $apprenant['nomDeNaissance'],
            'lib_pr1_ind' => $apprenant['prenom'],
            'lib_pr2_ind' => $apprenant['deuxiemePrenom'] ?? null,
            'lib_pr3_ind' => $apprenant['troisiemePrenom'] ?? null,
            'email' => $apprenant['mailPersonnel'] ?? null,
            'dat_nai_per' => $naissance['dateDeNaissance'],
            //'codepaysnationalite' => $naissance['nationalite'], // todo : colonne 'codepaysnationalite' obsolète
            'cod_pay_nat' => $naissance['nationalite'],
            'lib_nat' => $naissance['libelleNationalite'] ?? null,
            'supann_id' => null, // todo : quid ?
            'source_insert_date' => null,
            /*
            'code' => $this->>val($apprenant, 'code')], // code apprenant pegase
            'INE' => $this->>val($apprenant, 'INE')],
            'deuxieme_prenom' => $this->>val($apprenant, 'deuxiemePrenom')],
            'genre' => $this->>val($apprenant, 'genre')],
            'mail_personnel' => $this->>val($apprenant, 'mailPersonnel')],
            'mail_urgence' => $this->>val($apprenant, 'mailUrgence')],
            'nom_de_naissance' => $this->>val($apprenant, 'nomDeNaissance')],
            'nom_usuel' => $this->>val($apprenant, 'nomUsuel')],
            'prenom' => $this->>val($apprenant, 'prenom')],
            'prenom_usage' => $this->>val($apprenant, 'prenomUsage')],
            'telephone_personnel' => $this->>val($apprenant, 'telephonePersonnel')],
            'telephone_urgence' => $this->>val($apprenant, 'telephoneUrgence')],
            'troisieme_prenom' => $this->>val($apprenant, 'troisiemePrenom')],
            //
            'commune_de_naissance' => $naissance['communeDeNaissance'],
            'commune_de_naissance_etranger' => $naissance['communeDeNaissanceEtranger'],
            'date_d_obtention_de_la_deuxieme_nationalite' => $naissance['dateDObtentionDeLaDeuxiemeNationalite'],
            'date_de_naissance' => $naissance['dateDeNaissance'],
            'deuxieme_nationalite' => $naissance['deuxiemeNationalite'],
            'libelle_commune_de_naissance' => $naissance['libelleCommuneDeNaissance'],
            'libelle_deuxieme_nationalite' => $naissance['libelleDeuxiemeNationalite'],
            'libelle_nationalite' => $naissance['libelleNationalite'],
            'libelle_pays_de_naissance' => $naissance['libellePaysDeNaissance'],
            'nationalite' => $naissance['nationalite'],
            'pays_de_naissance' => $naissance['paysDeNaissance'],
            */
        ]);
    }

    protected function extractSourceCode(stdClass $object): string
    {
        /** @var array $apprenant */
        $apprenant = $object->apprenant;
        Assert::keyExists($apprenant, 'INE', "Clé %s introuvable dans les données apprenant");

        $ine = $apprenant['INE'];
        Assert::notEmpty($ine, "L'INE dans les données apprenant ne doit pas être vide");

        return $ine;
    }
}