<?php

namespace SygalApi\V1\Rest\InscriptionAdministrative\Extractor;

use stdClass;
use SygalApi\V1\Extractor\AbstractExtractor;
use Webmozart\Assert\Assert;

class DoctorantExtractor extends AbstractExtractor
{
    public function extract(object $object): array
    {
        $data = parent::extract($object);

        /** @var array $apprenant */
        $apprenant = $object->apprenant;

        $individuSourceCode = $apprenant['INE'];

        return array_merge($data, [
            'individu_id' => $individuSourceCode,
            'code_apprenant_in_source' => $apprenant['code'], // id métier de l'apprenant dans la source d'import = code apprenant pegase
            //
            'ine' => $apprenant['INE'],
            'source_insert_date' => null,
        ]);
    }

    protected function extractSourceCode(stdClass $object): string
    {
        /** @var array $apprenant */
        $apprenant = $object->apprenant;
        Assert::keyExists($apprenant, 'INE', "Clé %s introuvable dans les données apprenant");

        $ine = $apprenant['INE'];
        Assert::notEmpty($ine, "L'INE dans les données apprenant ne doit pas être vide");

        return $ine;
    }
}