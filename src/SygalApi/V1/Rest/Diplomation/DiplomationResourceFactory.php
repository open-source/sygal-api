<?php
namespace SygalApi\V1\Rest\Diplomation;

use Psr\Container\ContainerInterface;

class DiplomationResourceFactory
{
    public function __invoke(ContainerInterface $container): DiplomationResource
    {
        return new DiplomationResource();
    }
}
