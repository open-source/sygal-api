<?php

namespace SygalApi\Validator\OpenApi;

use Laminas\Http\Request;
use Laminas\Psr7Bridge\Psr7ServerRequest;
use Laminas\Validator\AbstractValidator;
use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
use League\OpenAPIValidation\PSR7\ServerRequestValidator;
use Webmozart\Assert\Assert;

class OpenApiValidator extends AbstractValidator
{
    public const INVALID_VALUE = 'invalidValue';

    private ServerRequestValidator $serverRequestValidator;
    protected string $explain;

    /** @var array<self::ERROR_*, non-empty-string> */
    protected array $messageTemplates = [
        self::INVALID_VALUE => 'Requete invalide : %explain%',
    ];

    /** @var array<string, string> */
    protected array $messageVariables = [
        'explain' => 'explain',
    ];

    public function setServerRequestValidator(ServerRequestValidator $serverRequestValidator): void
    {
        $this->serverRequestValidator = $serverRequestValidator;
    }

    /**
     * @param \Laminas\Http\Request $value
     */
    public function isValid($value): bool
    {
        Assert::isInstanceOf($value, Request::class);
        $request = $value;

        $psr7ServerRequest = Psr7ServerRequest::fromLaminas($request);
        try {
            $this->serverRequestValidator->validate($psr7ServerRequest);
        } catch (ValidationFailed $e) {
            $this->explain = $this->generateMessageFromException($e);
            $this->error(self::INVALID_VALUE);

            return false;
        }

        return true;
    }

    private function generateMessageFromException(ValidationFailed $e): string
    {
        $message = $e->getMessage();
        while ($e = $e->getPrevious()) {
            $message .= PHP_EOL . $e->getMessage();
        }

        return $message;
    }
}