<?php

namespace SygalApi\Validator\OpenApi;

use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

class OpenApiValidatorFactory implements AbstractFactoryInterface
{
    protected array $config;
    protected string $configKey = 'openapi_validator_specs';

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function canCreate(ContainerInterface $container, $requestedName): bool
    {
        $config = $this->getConfig($container);
        if (empty($config)) {
            return false;
        }

        return isset($config[$requestedName]);
    }

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): OpenApiValidator
    {
        $config = $this->getConfig($container);
        $config = $config[$requestedName];

        Assert::keyExists(
            $config,
            'oas_file_format',
            __CLASS__ . " : Le format du fichier de specification OpenAPI doit être spécifié par la clé %s"
        );
        Assert::keyExists(
            $config,
            'oas_file_path',
            __CLASS__ . " : Le chemin du fichier de specification OpenAPI doit être spécifié par la clé %s"
        );

        Assert::inArray(
            $config['oas_file_format'],
            ['yaml', 'json'],
            __CLASS__ . ' : %s ne fait pas partie des formats de schémas de validation supportés (%2$s)'
        );
        Assert::fileExists(
            $config['oas_file_path'],
            __CLASS__ . ' : Le fichier de spécification OpenAPI %s n\'existe pas, si ?'
        );

        $filePath = realpath($config['oas_file_path']);

        $builder = match ($config['oas_file_format']) {
            'yaml' => (new ValidatorBuilder)->fromYamlFile($filePath),
            'json' => (new ValidatorBuilder)->fromJsonFile($filePath),
        };
        $serverRequestValidator = $builder->getServerRequestValidator();

        // Si 'servers' nous emmerde, possibilité d'instancier un Schema et de l'élaguer :
//        $schema = (new YamlFileFactory($yamlFile))->createSchema();
//        unset($schema->servers);
//        $validator = (new ValidatorBuilder)->fromSchema($schema)->getServerRequestValidator();

        $validator = new OpenApiValidator();
        $validator->setServerRequestValidator($serverRequestValidator);

        return $validator;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getConfig(ContainerInterface $services): array
    {
        if (isset($this->config)) {
            return $this->config;
        }

        if (! $services->has('config')) {
            $this->config = [];

            return $this->config;
        }

        $config = $services->get('config');
        if (! isset($config[$this->configKey])) {
            $this->config = [];

            return $this->config;
        }

        $this->config = $config[$this->configKey];

        return $this->config;
    }
}