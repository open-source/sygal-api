<?php

namespace SygalApi\Validator\OpenApi;

use Laminas\Validator\ValidatorPluginManager;
use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

class OpenApiContentValidationManagerFactory
{
    protected array $config;
    protected string $configKey = 'openapi_content_validation';

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $name, ?array $options = null): OpenApiContentValidationManager
    {
        $config = $this->getConfig($container);

        return new OpenApiContentValidationManager($config);
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getConfig(ContainerInterface $services): array
    {
        if (isset($this->config)) {
            return $this->config;
        }

        $config = $services->get('config');
        Assert::keyExists($config, $this->configKey, "La clé %s doit exister dans la config");

        $this->config = $config[$this->configKey];

        /** @var \Laminas\Validator\ValidatorPluginManager $validatorPluginManager */
        $validatorPluginManager = $services->get(ValidatorPluginManager::class);

        foreach ($this->config as $controller => $validator) {
            $this->config[$controller] = $validatorPluginManager->get($validator);
        }

        return $this->config;
    }
}