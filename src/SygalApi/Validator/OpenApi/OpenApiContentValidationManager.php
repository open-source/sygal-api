<?php

namespace SygalApi\Validator\OpenApi;

use Laminas\ApiTools\ApiProblem\ApiProblem;
use Laminas\ApiTools\ContentValidation\ContentValidationListener;
use Laminas\EventManager\EventManagerInterface;
use Laminas\EventManager\ListenerAggregateInterface;
use Laminas\EventManager\ListenerAggregateTrait;
use Laminas\Mvc\MvcEvent;

class OpenApiContentValidationManager implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var array 'controller' => OpenApiValidator
     */
    private array $config;

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->getSharedManager()->attach(
            ContentValidationListener::class,
            ContentValidationListener::EVENT_BEFORE_VALIDATE,
            [$this, 'beforeValidate']
        );
    }

    public function beforeValidate(MvcEvent $event): ?ApiProblem
    {
        $controller = $event->getRouteMatch()->getParam('controller');

        if (!isset($this->config[$controller])) {
            return null;
        }

        /** @var \SygalApi\Validator\OpenApi\OpenApiValidator $validator */
        $validator = $this->config[$controller];

        /** @var \Laminas\Http\Request $request */
        $request = $event->getRequest();

        if (!$validator->isValid($request)) {
            return new ApiProblem(
                422,
                'Echec Validation OpenApi',
                'https://spec.openapis.org/oas/v3.0.1',
                "Unprocessable Entity",
                ['validation_messages' => implode(PHP_EOL, $validator->getMessages())]
            );
        }

        return null;
    }
}